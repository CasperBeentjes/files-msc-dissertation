import pdb # debug
import sys
import os
from SmalePolynomial import *
sys.path.append(os.path.dirname(os.getcwd()))
from math import *
from cmath import exp
from DeflateScalar import *
from NewtonScalar import *
import numpy as np
import colorsys
import multiprocessing
from functools import partial
from PIL import Image, ImageDraw

## Define polynomial
# wilkinson polynomial
M = 15
polyroots = range(1,M+1)
# Custom polynomial with given roots
a = 0.+1j
b = 0.5+0.5*1j
c = 0.2
polyroots = [1,a,b,c,-1]
#polyroots = [1,a,a.conjugate(),2,-2,-1]
#polyroots = [0.1*x for x in polyroots]
#polyroots = [1, exp(2*pi*1j/3), exp(-2*pi*1j/3)]
polyroots = np.asarray(polyroots)
f = np.poly1d(polyroots,True)
fprime = np.polyder(f)

power = 1 

## Domain size
xmax = 1
xmin = -xmax
ymax = 1
ymin = -ymax
x_step = 0.005
y_step = 0.005
x_size = int((xmax-xmin)/x_step+1)
y_size = int((ymax-ymin)/y_step+1)


roots = []
root_eps = 1e-6
max_it = 25

## Generate N colors for fractal
N = len(polyroots)
HSV_colors = [(x*1.0/N, 0.5, 0.5) for x in range(N)]
#RGB_colors = map(lambda x: tuple(int(255*y) for y in colorsys.hsv_to_rgb(*x)), HSV_colors)

## Iterate over initial guesses
init_guesses = []
for i, x0 in enumerate(np.arange(xmin,xmax,x_step)):
    for j, y0 in enumerate(np.arange(ymax,ymin,-y_step)):
        init_guesses.append((x0,y0,i,j)) 

data = np.zeros( (x_size,y_size,3), dtype=np.uint8)

def NewtonFractalColor(init_guess):
    x0, y0, i, j = init_guess
    z0 = x0+1j*y0
    try:
        (r, rhist) = NewtonScalar(deflate_poly,z0,deflate_derivative_poly,args=(f,fprime,roots,power),nargout=2,maxiter=max_it)
        it_length = len(rhist)
        for k, root in enumerate(polyroots):
            if abs(r-root)<root_eps:
                color = tuple(int(255*it_length/max_it*y) for y in colorsys.hsv_to_rgb(*HSV_colors[k]))
                return (i,j,color)
    except RuntimeError:
        return (i,j,(255,255,255))

def WilkinsonFractalColor(init_guess,N_root=N):
    x0, y0, i, j = init_guess
    z0 = x0+1j*y0
    roots = []
    for k in range(N_root):
        try:
            r = NewtonScalar(deflate_poly,z0,deflate_derivative_poly,args=(f,fprime,roots,1),nargout=1,maxiter=max_it)
            roots.append(r)
        except RuntimeError:
            if len(roots) == 0:
                return (i, j, (0,0,0))
            else:
                color = tuple(int(255*y) for y in colorsys.hsv_to_rgb(*HSV_colors[len(roots)]))
            return (i,j, color)
    return (i,j,(255,255,255)) 

## Parallel computing
pool = multiprocessing.Pool()
NewtonData = pool.map(NewtonFractalColor, init_guesses)
WilkinsonFractal = partial(WilkinsonFractalColor, N_root = N)
WilkinsonData = pool.map(WilkinsonFractal, init_guesses)
pool.close()
pool.join()

## Create image
# Newton fractal
img = Image.new('RGB',(x_size,y_size))
for pixel in NewtonData:
    i,j,color = pixel
    img.putpixel((i,j), color)
# Put RR-convergence ball overlay
rRR = int(2./3*1/x_step)
rSg = int(SmaleBound(f, a)/x_step)
rRRC = int(((sqrt(abs(a)**2+abs(1-a**2))-abs(a))/3.0)/x_step)
print rRRC, rSg
xa = int((1.0*a.real-xmin)/x_step)
ya = int((ymax-1.0*a.imag)/y_step)
xr1= int((1.0-xmin)/x_step)
yr1 = int((-ymin)/y_step)
xr2= int((-1.0-xmin)/x_step)
yr2 = int((-ymin)/y_step)
draw = ImageDraw.Draw(img)
draw.ellipse((xa-rRRC,ya-rRRC,xa+rRRC,ya+rRRC),outline='yellow')
draw.ellipse((xa-rSg,ya-rSg,xa+rSg,ya+rSg),outline='green')
draw.ellipse((xr1-rRR,yr1-rRR,xr1+rRR,yr1+rRR),outline='red')
draw.ellipse((xr2-rRR,yr2-rRR,xr2+rRR,yr2+rRR),outline='blue')
img.save('NewtonFractal.png',"PNG")
# Wilkinson fractal
img = Image.new('RGB',(x_size,y_size))
for pixel in WilkinsonData:
    i,j,color = pixel
    img.putpixel((i,j), color)
img.save('WilkinsonFractal.png',"PNG")
