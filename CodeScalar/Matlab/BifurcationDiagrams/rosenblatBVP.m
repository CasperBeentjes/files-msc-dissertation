%% AUTO vs FEniCS Rosenblat BVP
% Create matlab plots of the AUTO generated plots and the FEniCS
% generated plots
clear all; close all;
lw = 'Linewidth';
xmin = 0;
xmax = 200.0;
curdir = pwd;
%% FEniCS data (1st entry is parameter, 2nd is functional)
cd ../../../FEniCS/auto07p_comparison/BVP_Rosenblat

branches = load('rosenblatBVP.mat');

f1 = figure();
color = ['k','r','b'];
hold on
fields = fieldnames(branches);
for i = 2:numel(fields)
    branch = branches.(fields{i});
    g1 = plot(branch(1,:),branch(2,:),color(1),lw,2);
end
%% AUTO data 
% 1st entry is parameter, 2nd is L2-norm u&u', 3rd is max u
% 4th is max u', 5th is correct L2-norm, 6th is directed L2-nrom
% 7th is min u
cd(curdir)
cd ../../../auto-07p/BVP_Rosenblat

branches = load('rosenblatBVP.mat');
fields = fieldnames(branches);
step = 50;
for i = 1:numel(fields)
    branch = branches.(fields{i});
    g2 = plot(branch(1,1:step:end),branch(3,1:step:end),'r.',lw,3);
end

cd(curdir)
%% Plotting adaption
xlim([xmin, xmax])
xlabel('\mu')
ylabel('\|u\|_{L_2}')
box on
legend([g1,g2],'Deflation','AUTO-07P','Location','NorthWest')
cleanfigure()
matlab2tikz('rosenblatBVP.tikz')

