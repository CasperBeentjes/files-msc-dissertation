"""
Deflation of scalar functions methods

Shifted norm deflation:
f(x) -> (1/|x-root|^p + shift)*f(x)
Polynomial division (Wilkinson deflation):
f(x) -> (1/(x-root)^p + shift)*f(x)
"""
from __future__ import division

# Deflate a function
def deflate(x,f,fprime,root,p=1.0,shift=0.0):
    factor = 1.
    for r in root:
        factor *= abs(x-r)**(-p)+shift
    return f(x)*factor

sgn = lambda x: 1.*(x>0) -1.* (x<0)
# Derivative of a deflated function
def deflate_derivative(x,f,fprime,root,p=1.0,shift=0.0):
    factors = []
    dfactors = []
    deta = 0.
    for r in root:
        factor = abs(x-r)**(-p) + shift
        dfactor = (-p)*abs(x-r)**(-p-1)
        factors.append(factor)
        dfactors.append(dfactor)
    if factors:
        eta = reduce(lambda x, y: x * y, factors) 
    else:
        eta = 0.
    for i,r in enumerate(root):
        deta+=(eta/factors[i])*dfactors[i]*sgn(x-r)
    return f(x)*deta+deflate(x,fprime,fprime,root,p,shift)

"""
Polynomial division (no absolute values etc.)
"""
def deflate_poly(x,f,fprime,root,p=1.0):
    factor = 1.
    for r in root:
        factor *= (x-r)**(-p)
    return f(x)*factor

def deflate_derivative_poly(x,f,fprime,root,p=1.0):
    factors = []
    dfactors = []
    deta = 0.
    for r in root:
        factor = (x-r)**(-p)
        dfactor = (-p)*(x-r)**(-p-1)
        factors.append(factor)
        dfactors.append(dfactor)
    if factors:
        eta = reduce(lambda x, y: x * y, factors) 
    else:
        eta = 0.
    for i,r in enumerate(root):
        deta+=(eta/factors[i])*dfactors[i]
    return f(x)*deta+deflate_poly(x,fprime,fprime,root,p)
