%% Convergence Regions for complex Quadratic polynomial plot
% Plot the result of the local convergence theorems for any complex
% quadratic polynomial and Newton's method using affine remapping
% Given f(x) = (x-1)(x+1)(x-a) we plot the region A such that if
% a in A then there exists a point that converges to all roots
close all;
clear all;
cmap = colormap(parula(4));
%% Grid generation
[x,y] = meshgrid(-2.1:0.01:2.1);
%%
f1 = 2 + (sqrt(abs(x+1i*y).^2+abs(1-(x+1i*y).^2))-abs(x+1i*y))-3*abs(1-x-1i*y);
[c1,h1] = contour(x,y,f1,[0 0],'b');
c1 = c1(:,2:end);
delete(h1)
c1(:,(c1(1,:)+1).^2+c1(2,:).^2>4)=[];
%plot(c1(1,:),c1(2,:),'b')
hold on
%%
f2 = 2 + (sqrt(abs(-x+1i*y).^2+abs(1-(-x+1i*y).^2))-abs(-x+1i*y))-3*abs(1+x-1i*y);
[c2,h2] = contour(x,y,f2,[0 0],'b');
delete(h2)
c2 = c2(:,2:end);
c2(:,(c2(1,:)-1).^2+c2(2,:).^2>4)=[];
%plot(c2(1,:),c2(2,:),'b')
%% Unit circle
f3 = x.^2+y.^2-1;
[c3,h3] = contour(x,y,f3,[0 0],'k--');
delete(h3)
%plot(c3(1,2:end),c3(2,2:end),'k--');
%%
g1 = (x-1).^2+y.^2-4;
[cg1,hg1] = contour(x,y,g1,[0 0],'k--');
cg1 = cg1(:,2:end);
cg1(:,cg1(1,:)>0) = [];
%plot(cg1(1,:),cg1(2,:),'k--');
delete(hg1)
%%
g2 = (x+1).^2+y.^2-4;
[cg2,hg2] = contour(x,y,g2,[0 0],'k--');
cg2 = cg2(:,2:end);
cg2(:,cg2(1,:)<0) = [];
plot([cg1(1,:) cg2(1,:)],[cg1(2,:) cg2(2,:)],'k--');
delete(hg2)
%% Root area
% right
c4 = cg2; c4(:,c4(1,:)<max(c1(1,:)))=[];
t = [c1 c4(:,end:-1:1)];
fill(t(1,:),t(2,:),'b','EdgeColor','none')
% left
c5 = cg1; c5(:,c5(1,:)>min(c2(1,:)))=[];
%plot(c5(1,:),c5(2,:),'r')
t = [c2 c5(:,end:-1:1)];
fill(t(1,:),t(2,:),'b','EdgeColor','none')
%% Plot properties
plot([1,-1],[0,0],'rx')
xlabel('Re(x)')
ylabel('Im(x)')
grid on

axis equal
ylim([-1.8, 1.8])
xlim([-1.1, 1.1])
%legend([h1,h3],{'Newton-Kantorovich','Rall-Rheinboldt'},'Position',[0.417,0.75,0.2,0.15])