#import scipy.optimize as so
from math import *
from DeflateScalar import *
from NewtonScalar import *
import matplotlib.pyplot as plt
import numpy as np
import copy
 
# sine
f = lambda x: np.sin(pi*x)
fprime = lambda x: pi*np.cos(pi*x)
# wilkinson polynomial
M = 10 
polyroots = range(1,M+1)
#polyroots = [1,-2,1j,-1j]
a = 0.05
polyroots = [1,-1,a]
polyroots = np.asarray(polyroots)
f = np.poly1d(polyroots,True)
fprime = np.polyder(f)



x0 = 0.9
power = 1
shift = 0
roots = []
Nit = len(polyroots)

for n in range(Nit):
    (r, rhist) = NewtonScalar(deflate,x0,deflate_derivative,args=(f,fprime,roots,power,shift),nargout=2,maxiter=100)
    roots.append(r)
    print rhist
    print roots

rr = copy.deepcopy(roots)
root.sort()
eps = 1e-3
nx = 50
xmax = roots[-1]+1 
xmin = roots[0]-1
x = np.linspace(xmin,roots[0]-eps,nx)
for i in range(len(roots)-1):
    x = np.append(x,np.linspace(roots[i]+eps,roots[i+1]-eps,nx))
x = np.append(x,np.linspace(roots[-1]+eps,xmax,nx))   

plt.close('all')
fs = 20
plt.rc('text', usetex=True)
plt.rc('font', family='serif')
fig, ax = plt.subplots(1)
ax.plot(x,f(x),'r',lw=2)
ax.plot(x,deflate(x,f,fprime,[rr[0]],power,shift),'k',lw=2)
#ax.plot(x,deflate_derivative(x,f,fprime,[rr[0]],power,shift),'b',lw=2)
ax.plot(x0,0,'r*',lw=2)
ax.text(x0,0.1,'$x_0$',fontsize=fs)
plt.xlabel('$x$',fontsize=fs)
plt.ylabel('$y(x)$',fontsize=fs)
#plt.ylim(ymin=-100,ymax=200)
ax.tick_params(labelsize=fs)
ax.grid()
plt.show()
