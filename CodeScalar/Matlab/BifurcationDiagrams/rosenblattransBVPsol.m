%% AUTO vs FEniCS Rosenblat BVP solution
% Create matlab plots of the AUTO generated plots and the FEniCS
% generated plots
clear all; close all;
lw = 'Linewidth';
xmin = 0;
xmax = 1.0;
curdir = pwd;
%% FEniCS data (1st entry is parameter, 2nd is functional)
cd ../../../FEniCS/auto07p_comparison/BVP_Rosenblat/TransformedSystem

branches = load('solrosenblattransBVP-1.mat');

f1 = figure();
color = ['g','r','b'];
hold on
fields = fieldnames(branches);
for i = 1:numel(fields)
    branch = branches.(fields{i});
    g1 = plot(branch{1},branch{2},color(i),lw,2);
end
cd(curdir)
%% Plotting adaption
xlim([xmin, xmax])
xlabel('x')
ylabel('u')
box on
%legend([g1,g2],'Deflation','AUTO-07P','Location','NorthWest')
cleanfigure()
matlab2tikz('rosenblattransBVPsol-1.tikz')

