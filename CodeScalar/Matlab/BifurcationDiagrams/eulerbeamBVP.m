%% AUTO vs FEniCS Euler elastica BVP
% Create matlab plots of the AUTO generated plots and the FEniCS
% generated plots
clear all; close all;
lw = 'Linewidth';
xmin = 0.0;
xmax = 4.0;
mu = 0.0;
curdir = pwd;
%% FEniCS data (1st entry is parameter, 2nd is functional)
cd ../../../FEniCS/auto07p_comparison/EulerBeam
if mu == 0
    branches = load('eulerbeam0.0.mat');
else
    branches = load(['eulerbeam' num2str(mu) '.mat']);
end
f1 = figure();
color = ['k','r','b'];
hold on
fields = fieldnames(branches);
for i = 1:numel(fields)
    if fields{i}(7) == '0'
        continue
    end
    branch = branches.(fields{i});
    g1 = plot(branch(1,:)/pi,branch(2,:),color(1),lw,2);
end
%% AUTO data 
% 1st entry is parameter, 2nd is L2-norm u&u', 3rd is max u
% 4th is max u', 5th is correct L2-norm, 6th is directed L2-nrom
% 7th is min u
cd(pwd)
cd ../../../auto-07p/EulerBeam
if mu == 0
    branches = load('eulerbeam0.0.mat');
    fields = fieldnames(branches);
    step = 8;
    for i = 1:numel(fields)
        branch = branches.(fields{i});
        g2 = plot(branch(1,1:step:end),branch(6,1:step:end),'r.',lw,3);
    end
else
    branches = load(['eulerbeam' num2str(mu) '.mat']);
    fields = fieldnames(branches);
    step = 5;
    for i = numel(fields)-2:numel(fields)-2
        branch = branches.(fields{i});
        g2 = plot(branch(1,1:step:end),branch(6,1:step:end),'r.',lw,3);
    end
end


cd(curdir)
%% Plotting adaption
xlim([xmin, xmax])
xlabel('\lambda/\pi')
ylabel('\|u\|')
box on
legend([g1,g2],'Deflation','AUTO-07P','Location','NorthWest')
cleanfigure()
matlab2tikz(['eulerbeammu' num2str(mu) '.tikz'])

