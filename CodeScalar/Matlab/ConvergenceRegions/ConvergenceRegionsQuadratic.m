%% Convergence Regions for complex Quadratic polynomial plot
% Plot the result of the local convergence theorems for any complex
% polynomial (with distinct roots) and Newton's method using affine remapping
close all;
clear all;
cmap = colormap(parula(4));
%% Grid generation
[x,y] = meshgrid(-4:0.05:4);
%% Newton-Kantorovich
f = sqrt(4*x.^2.*y.^2./(x.^2+y.^2).^4+(1-(x.^2-y.^2)./(x.^2+y.^2).^2).^2);
[cnk,h] = contour(x,y,f,[1 1],'k','LineWidth',1);
xnk1 = cnk(1,2:size(cnk,2)/2);
xnk2 = cnk(1,size(cnk,2)/2+2:end);
ynk1 = cnk(2,2:size(cnk,2)/2);
ynk2 = cnk(2,size(cnk,2)/2+2:end);
h1 = area(xnk1,ynk1,'ShowBaseLine','off','EdgeColor',cmap(1,:));
hold on
h2 = area(xnk2,ynk2,'ShowBaseLine','off','EdgeColor',cmap(1,:));
h1.FaceColor = cmap(1,:);
h2.FaceColor = cmap(1,:);
delete(h)
%% Rall-Rheinboldt & Refined Newton-Mysovskikh
radius = 2/3;
f = sqrt((x-1).^2+y.^2);
[crr,h] = contour(x,y,f,[radius radius],'k','LineWidth',1);
xrr1 = crr(1,2:end);
yrr1 = crr(2,2:end);
h3 = area(xrr1,yrr1,'ShowBaseLine','off','EdgeColor',cmap(2,:));
f = sqrt((x+1).^2+y.^2);
delete(h)
[crr,h] = contour(x,y,f,[radius radius],'k','LineWidth',1);
xrr2 = crr(1,2:end);
yrr2 = crr(2,2:end);
h4 = area(xrr2,yrr2,'ShowBaseLine','off','EdgeColor',cmap(2,:));
h3.FaceColor = cmap(2,:);
h4.FaceColor = cmap(2,:);
delete(h)
%% Smale gamma
radius = (5-sqrt(17))/2;
f = sqrt((x-1).^2+y.^2);
[csg,h] = contour(x,y,f,[radius radius],'k','LineWidth',1);
xsg1 = csg(1,2:end);
ysg1 = csg(2,2:end);
h5 = area(xsg1,ysg1,'ShowBaseLine','off','EdgeColor',cmap(3,:));
delete(h)
f = sqrt((x+1).^2+y.^2);
[csg,h] = contour(x,y,f,[radius radius],'k','LineWidth',1);
xsg2 = csg(1,2:end);
ysg2 = csg(2,2:end);
h6 = area(xsg2,ysg2,'ShowBaseLine','off','EdgeColor',cmap(3,:));
h5.FaceColor = cmap(3,:);
h6.FaceColor = cmap(3,:);
delete(h)
%% Smale alpha
a0 = 0.1507;
f = sqrt(4*x.^2.*y.^2./(x.^2+y.^2).^4+(1-(x.^2-y.^2)./(x.^2+y.^2).^2).^2);
[csa,h] = contour(x,y,f,[4*a0 4*a0],'k','LineWidth',1);
xsa1 = csa(1,2:size(csa,2)/2);
xsa2 = csa(1,size(csa,2)/2+2:end);
ysa1 = csa(2,2:size(csa,2)/2);
ysa2 = csa(2,size(csa,2)/2+2:end);
h7 = area(xsa1,ysa1,'ShowBaseLine','off','EdgeColor',cmap(4,:));
hold on
h8 = area(xsa2,ysa2,'ShowBaseLine','off','EdgeColor',cmap(4,:));
h7.FaceColor = cmap(4,:);
h8.FaceColor = cmap(4,:);
delete(h)
%% Plot properties
plot([1,-1],[0,0],'rx')
xlabel('Re(x)')
ylabel('Im(x)')
grid on
axis equal
legend([h1,h3,h5,h7],{'Newton-Kantorovich','Rall-Rheinboldt','Smale gamma','Smale alpha'},'Position',[0.417,0.75,0.2,0.15])