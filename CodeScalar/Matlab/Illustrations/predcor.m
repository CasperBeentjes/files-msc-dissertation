%% Sketch of arclength continuation
% use as function y = sqrt(x)
clear all; close all;
x = 0.15:0.01:4;
lw = 'Linewidth';
plot(x,sqrt(x),'k',lw,2)
xlim([0-0.5 4+0.5])
ylim([0-0.5 sqrt(4)+0.5])
% tangent line
x0 = 0.5; % starting point
xp = 2.5; % end point
f = @(x,x0) x./(2*sqrt(x0)) + sqrt(x0)/2;
yp = f(xp,x0);
t = x0:0.01:xp;
hold on
plot(t,f(t,x0),'b',lw,2)
% destination on curve
xc = 2.6;
yc = sqrt(xc);
% create a wiggly path for the corrector step
corpath = xp:0.01:xc;
t = 0.05*(-0.5 + rand(length(corpath),1));
t(1)=0;t(end)=0;
fpred = @(x) yp + (yc-yp)/(xc-xp)*(x-xp);
plot(corpath+t',fpred(corpath),'k:',lw,2)
% red overlay on curve
ds = x0:0.01:xc;
plot(ds,sqrt(ds),'r',lw,2)
% plot special points
plot(x0,sqrt(x0),'k*',lw,2)
plot(xp,yp,'b*',lw,2)
plot(xc,yc,'r*',lw,2)
% plot extras
xlabel('\Large $\lambda$')
ylabel('\Large $J(u)$')
set(gca,'YTick',[]);
set(gca,'XTick',[]);
% save to Tikz
cleanfigure()
matlab2tikz('predcor.tikz')

