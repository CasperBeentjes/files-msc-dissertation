# README #

### What is this repository for? ###

* Files used for MSc dissertation project *"Computing Bifurcation Diagrams with Deflation"* by Casper Beentjes 

### AUTO-07p folder ###

* Test of numerical continuation software on ODEs, BVPs and 1D parabolic PDEs with both connected and disconnected branches

* To install AUTO-07P visit http://indy.cs.concordia.ca/auto/

### CodeScalar ###

* Some Python code on deflation of scalar functions and Wilkinson deflation of polynomials (including creation of Newton fractals)

* Matlab code to generate part of the plots that do not use FEniCS code

### Dissertation folder ###

* Dissertation PDF

### Note on FEniCS code ###

* The FEniCS implementation of deflation + continuation is not yet available

### Who do I talk to? ###

* Casper Beentjes [beentjes@maths.ox.ac.uk](mailto:beentjes@maths.ox.ac.uk)