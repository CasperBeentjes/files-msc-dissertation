%% AUTO vs FEniCS Transformed Rosenblat ODE
% Create matlab plots of the AUTO generated plots and the FEniCS
% generated plots
clear all; close all;
lw = 'Linewidth';
xmin = -2.0;
xmax = 1.0;
curdir = pwd;
%% FEniCS data (1st entry is parameter, 2nd is functional)
cd ../../../FEniCS/auto07p_comparison/Rosenblat/TransformedSystem

branches = load('rosenblattrans.mat');

f1 = figure();
color = ['k','r','b'];
hold on
fields = fieldnames(branches);
for i = 2:numel(fields)
    branch = branches.(fields{i});
    g1 = plot(branch(1,:),branch(2,:),color(1),lw,2);
end
%% AUTO data (1st entry is parameter, 2nd is norm, 3rd is x)
cd(curdir)
cd ../../../auto-07p/Rosenblat/TransformedSystem

branches = load('rosenblattrans.mat');
fields = fieldnames(branches);
step = 2;
for i = 1:numel(fields)
    branch = branches.(fields{i});
    g2 = plot(branch(1,1:step:end),branch(3,1:step:end),'r.',lw,3);
end

cd(curdir)
%% Plotting adaption
xlim([xmin, xmax])
xlabel('\lambda')
ylabel('u')
box on
legend([g1,g2],'Deflation','AUTO-07P','Location','NorthEast')
cleanfigure()
matlab2tikz('rosenblattransODE.tikz')

