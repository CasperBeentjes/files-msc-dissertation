%% Create matlab plots of the AUTO generated plots to export
%% to tikz
clear all; close all;
lw = 'Linewidth';
xmax = 2.0;
%% Perfect pitchfork
files = dir('no_impf*.data');
f1 = figure();
color = ['k','r','b'];
i = 1;
for file = files'
    branch = dlmread(file.name);
    plot(branch(:,1),branch(:,6),color(1),lw,2)
    hold on
    i = i + 1;
end
% plot bifurcation point
tmp = branch(:,6);
[u, k] = min(branch(:,1));
plot(u,tmp(k),'r*')
xlim([0,xmax])
cleanfigure()
matlab2tikz('perfectpitchfork.tikz')
%% Imperfect pitchfork
files = dir('impf*.data');
f2 = figure();
i = 1;
for file = files'
    branch = dlmread(file.name);
    plot(branch(:,1),branch(:,6),color(1),lw,2)
    hold on
    i = i + 1;
end
% plot bifurcation point
branch = dlmread('impf1.data');
tmp = branch(:,6);
[u, k] = min(branch(:,1));
plot(u,tmp(k),'r*')
xlim([0,xmax])
cleanfigure()
matlab2tikz('imperfectpitchfork.tikz')
