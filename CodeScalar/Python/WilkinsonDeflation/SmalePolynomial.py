## Calculate Smale's gamma function for polynomials
# use np.poly1d
from math import factorial, sqrt
import numpy as np

def gamma(p, x):
    n = len(p) # degree of polynomial
    if n <= 1:
        return 0
    pdiff = np.polyder(p)(x)
    factor =[]
    for k in range(2,n+1):
        factor.append((abs(1.0*np.polyder(p,k)(x)/pdiff)/factorial(k))**(1.0/(k-1)))
    gamma = max(factor)
    return gamma

def SmaleBound(p, x):
    return (5.0-sqrt(17))/(4*gamma(p, x))

if __name__ == "__main__":
    a = 1j
    p = np.poly1d([1,-1,a],True)
    print SmaleBound(p,1)
    print SmaleBound(p,-1)
    print SmaleBound(p,a)
