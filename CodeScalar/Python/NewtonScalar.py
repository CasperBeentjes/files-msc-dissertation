from __future__ import division
import warnings
def NewtonScalar(func, x0, fprime, args=(), tol=1.48e-8, maxiter=50, nargout = 1):
    """
    Adapation of scipy optimisation Newton method which can return the convergence history
    """
    if tol <= 0:
        raise ValueError("tol too small (%g <= 0)" % tol)
    p0 = 1.0 * x0
    phistory = [p0]
    for iter in range(maxiter):
        myargs = (p0,) + args
        fder = fprime(*myargs)
        if fder == 0:
            msg = "derivative was zero."
            ## Raise error upon zero derivative
            raise RuntimeError(msg)
            ## Raise warning upon zero derivative
#            warnings.warn(msg, RuntimeWarning)
#            return p0
        fval = func(*myargs)
        # Newton step
        p = p0 - fval / fder
        phistory.append(p)
        if abs(p - p0) < tol:
            if nargout == 1:
                return p
            if nargout == 2:
                return p, phistory
        p0 = p
    msg = "Failed to converge after %d iterations, value is %s" % (maxiter, p)
    raise RuntimeError(msg)
