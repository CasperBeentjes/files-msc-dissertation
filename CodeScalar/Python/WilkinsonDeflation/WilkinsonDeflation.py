## Look at Wilkinson deflation on polynomials
## Use numpy poly class, give in roots, numpy will
## take care of the derivatives
import sys
import os
# Get modules from parent directory
sys.path.append(os.path.dirname(os.getcwd()))
from math import *
from DeflateScalar import *
from NewtonScalar import *
import matplotlib.pyplot as plt
import numpy as np
import copy
 
## wilkinson polynomial
M = 10
polyroots = range(1,M+1)
## Predefined roots
a = 0.05
polyroots = [1,-1,a]
# Numpy poly class
polyroots = np.asarray(polyroots)
f = np.poly1d(polyroots,True)
fprime = np.polyder(f)

x0 = 2
power = 1 
roots = []
Nit = len(polyroots)

## iterate over number of roots of polynomial, use Wilkinson deflation
for n in range(Nit):
    (r, rhist) = NewtonScalar(deflate_poly,x0,deflate_derivative_poly,args=(f,fprime,roots,power),nargout=2,maxiter=50)
    roots.append(r)
    print 'Newton iterates: ' 
    print rhist
    print 'Roots found so far: '
    print roots

## Plot 
rr = copy.deepcopy(roots)
roots.sort()
eps = 1e-3
nx = 50
xmax = roots[-1]+1 
xmin = roots[0]-1
x = np.linspace(xmin,roots[0]-eps,nx)
for i in range(len(roots)-1):
    x = np.append(x,np.linspace(roots[i]+eps,roots[i+1]-eps,nx))
x = np.append(x,np.linspace(roots[-1]+eps,xmax,nx))   

plt.close('all')
fs = 20
plt.rc('axes', color_cycle=['r','g','b','k','y'])
plt.rc('text', usetex=True)
plt.rc('font', family='serif')
plt.rc('lines', lw = 3)
fig, ax = plt.subplots(1)
ax.plot(x,f(x))
for i in range(1,len(roots)): 
    ax.plot(x,deflate_poly(x,f,fprime,rr[0:i],power))
ax.plot(x0,0,'r*',lw=2)
ax.text(x0,0.1,'$x_0$',fontsize=fs)
plt.xlabel('$x$',fontsize=fs)
plt.ylabel('$y(x)$',fontsize=fs)
ax.tick_params(labelsize=fs)
ax.grid()
plt.show()
