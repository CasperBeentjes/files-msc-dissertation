%% Sketch the overlapping Newton balls idea for convergence theorems
%% which work in the initial guess frame
clear all; close all;
xmin = -4;
xmax = 6;
ymin = -3;
ymax = 4.5;
[x,y] = meshgrid(xmin:0.1:xmax,ymin:0.1:ymax);
lw = 'Linewidth';
% roots
x1 = -2;
y1 = 1;
x2 = 2;
y2 = -1;
% initial guess
x0 = -0.3;
y0 = 0.7;
%% before deflation
figure()
plot([x1,x2],[y1,y2],'r*',lw,3)
hold on
plot(x0,y0,'k*',lw,3)
% ball around x0
f1 = (x-x0).^2+(y-y0).^2;
r1 = 4;
[c1,h1] = contour(x,y,f1,[r1 r1],'b:');
delete(h1);
plot(c1(1,2:end),c1(2,2:end),'b--',lw,2)
% convergence path
t = linspace(x0,x1,40);
rnd = 0.12*(-0.5 + rand(length(t),1));
rnd(1)=0;rnd(end)=0;
f = @(t) y0 + (y1-y0)/(x1-x0)*(t-x0);
plot(t,f(t)+rnd','k--',lw,1)

set(gca,'YTick',[]);
set(gca,'XTick',[]);
axis equal
xlim([xmin,xmax])
ylim([ymin,ymax])
cleanfigure()
matlab2tikz('beforedeflation.tikz')
%% after deflation
figure()
plot(x2,y2,'r*',lw,3)
hold on
plot(x1,y1,'rx',lw,3)
plot(x0,y0,'k*',lw,3)

% old ball around x0
plot(c1(1,2:end),c1(2,2:end),'b--',lw,2)
% new ball around x0
f1 = (x-x0).^2+(y-y0).^2;
r1 = 9;
[c1,h1] = contour(x,y,f1,[r1 r1],'b:');
delete(h1);
plot(c1(1,2:end),c1(2,2:end),'b--',lw,2)

% convergence path
t = linspace(x0,x2,40);
rnd = 0.2*(-0.5 + rand(length(t),1));
rnd(1)=0;rnd(end)=0;
f = @(t) y0 + (y2-y0)/(x2-x0)*(t-x0);
plot(t,f(t)+rnd','k--',lw,1)

axis equal
xlim([xmin,xmax])
ylim([ymin,ymax])

set(gca,'YTick',[]);
set(gca,'XTick',[]);
cleanfigure()
matlab2tikz('afterdeflation.tikz')

